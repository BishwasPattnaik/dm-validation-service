package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import groovy.lang.Writable;

public class ApachePOIExcelRead {

  public static List<String> tablenames = new ArrayList();
  public static List<Double> tablesizes = new ArrayList<>();
  public static List<Double> diffofsizes = new ArrayList<>();
  public static List<Double> tablesizesint = new ArrayList<>();
  public static List<Double> totalrowsint = new ArrayList<>();
  public static List<String> lesserDataTable = new ArrayList<>();
  public static List<String> biggerDataTable = new ArrayList<>();
  public static List<String> sameSizeDataTable = new ArrayList<>();

  public List<String> getTableName(String filelocation) throws IOException {
    String tablename = null;
    FileInputStream excelFile = new FileInputStream(new File(filelocation));
    Workbook workbook = new XSSFWorkbook(excelFile);
    Sheet datatypeSheet = workbook.getSheetAt(0);
    int rowCount = datatypeSheet.getLastRowNum() - datatypeSheet.getFirstRowNum();
    for (int i = 1; i < rowCount + 1; i++) {
      Row row = datatypeSheet.getRow(i);
      tablename = row.getCell(0).getStringCellValue();
      tablenames.add(tablename);
    }
    return tablenames;
  }

  public void writeXLSXFile(String filelocation, List<Double> tables)
      throws IOException, ClassNotFoundException, SQLException, InterruptedException {
    try {
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Retrieve the row and check for null
      int lastrow = sheet.getLastRowNum();
      // Update the value of cell
      for (int i = 1; i <= lastrow; i++) {
        Row sheetrow = sheet.getRow(i);
        int lastcell = sheetrow.getPhysicalNumberOfCells();
        Cell cell = sheetrow.getCell(lastcell);
        if (cell == null) {
          cell = sheetrow.createCell(lastcell);
        }
        cell.setCellValue(tables.get(i - 1));
      }
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("The cell is being updated.");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void writeXLSXFileInteger(String filelocation, List<Double> tables)
      throws IOException, ClassNotFoundException, SQLException, InterruptedException {
    try {
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Retrieve the row and check for null
      int lastrow = sheet.getLastRowNum();
      // Update the value of cell
      for (int i = 1; i <= lastrow; i++) {
        Row sheetrow = sheet.getRow(i);
        int lastcell = sheetrow.getPhysicalNumberOfCells();
        Cell cell = sheetrow.getCell(lastcell);
        if (cell == null) {
          cell = sheetrow.createCell(lastcell);
        }
        cell.setCellValue(tables.get(i - 1));
      }
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("The cell is being updated.");

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void createColumn(String filelocation, String columnname) {
    try {
      String prefix = getPrefix();
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Update the value of cell
      Row sheetrow = sheet.getRow(0);
      int lastcell = sheetrow.getPhysicalNumberOfCells();
      Cell cell = sheetrow.getCell(lastcell);
      if (cell == null) {
        cell = sheetrow.createCell(lastcell);
      }
      cell.setCellValue(columnname + prefix);
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println(columnname + " : column has been crreated successfully");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String getPrefix() {
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Date date = new Date();
    SimpleDateFormat mf = new SimpleDateFormat("MM");
    SimpleDateFormat df = new SimpleDateFormat("dd");
    SimpleDateFormat hr = new SimpleDateFormat("HH");
    SimpleDateFormat mm = new SimpleDateFormat("MM");
    String month = mf.format(date);
    String day = df.format(date);
    String hour = hr.format(date);
    String minute = mm.format(date);
    String prefix = year + "/" + month + "/" + day + " " + hour + ":" + minute;
    return prefix;
  }

  public void calculateDiff(String FILE_NAME)
      throws IOException, ClassNotFoundException, SQLException, InterruptedException {
    FileInputStream file = new FileInputStream(new File(FILE_NAME));
    Workbook workbook = new XSSFWorkbook(file);
    Sheet sheet = workbook.getSheetAt(0);
    // Update the value of cell
    int sr = sheet.getLastRowNum();
    Row firstrow = sheet.getRow(0);
    int lastcellnum = firstrow.getPhysicalNumberOfCells();
    System.out.println(lastcellnum);
    if (lastcellnum > 3) {
      if (lastcellnum <= 5) {
        for (int i = 1; i <= sr; i++) {
          Row sheetrow = sheet.getRow(i);
          int lastcell = sheetrow.getPhysicalNumberOfCells();
          Cell lastrunsize = sheetrow.getCell(lastcell - 4);
          Cell latestrunsize = sheetrow.getCell(lastcell - 2);
          double lastrun = Double.parseDouble(lastrunsize.toString());
          double latestrun = Double.parseDouble(latestrunsize.toString());
          double diff = latestrun - lastrun;
          diffofsizes.add(diff);
        }
      } else {
        for (int i = 1; i <= sr; i++) {
          Row sheetrow = sheet.getRow(i);
          int lastcell = sheetrow.getPhysicalNumberOfCells();
          Cell lastrunsize = sheetrow.getCell(lastcell - 5);
          Cell latestrunsize = sheetrow.getCell(lastcell - 2);
          double lastrun = Double.parseDouble(lastrunsize.toString());
          double latestrun = Double.parseDouble(latestrunsize.toString());
          double diff = latestrun - lastrun;
          diffofsizes.add(diff);
        }
      }
      createColumn(FILE_NAME, "Size Difference :");
      writeXLSXFile(FILE_NAME, diffofsizes);
    } else {
      System.out.println("There are no values to be compared");
    }
  }

  public void getCellValue(String filelocation) throws IOException {
    FileInputStream file = new FileInputStream(new File(filelocation));
    Workbook workbook = new XSSFWorkbook(file);
    Sheet sheet = workbook.getSheetAt(0);
    int sr = sheet.getLastRowNum();
    Row sheetrow = sheet.getRow(0);
    int mycell = sheetrow.getPhysicalNumberOfCells();
    for (int i = mycell - 1; i > 0; i--) {
      String value = sheetrow.getCell(i).getStringCellValue();
      if (value.contains("Size")) {
        System.out.println(value);
      }
    }
  }

  public void createSheet(String filelocation) throws IOException {
    FileInputStream file = new FileInputStream(new File(filelocation));
    Workbook workbook = new XSSFWorkbook(file);
    Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("test"));
    System.out.println(workbook.getNumberOfSheets());
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Date date = new Date();
    SimpleDateFormat mf = new SimpleDateFormat("MM");
    SimpleDateFormat df = new SimpleDateFormat("dd");
    SimpleDateFormat hr = new SimpleDateFormat("HH");
    SimpleDateFormat mm = new SimpleDateFormat("MM");
    String month = mf.format(date);
    String day = df.format(date);
    String prefix = year + "-" + month + "-" + day;
    if (day.contains("17")) {
      System.out.println("True");
      // workbook.createSheet();
      // workbook.createSheet("test");
    }
    // Sheet sheet = workbook.getSheetAt(0);
  }

  public void checkSize(String filelocation) throws IOException {
    FileInputStream file = new FileInputStream(new File(filelocation));
    Workbook workbook = new XSSFWorkbook(file);
    Sheet sheet = workbook.getSheetAt(0);
    // Update the value of cell
    int sr = sheet.getLastRowNum();
    Row firstrow = sheet.getRow(0);
    int lastcellnum = firstrow.getPhysicalNumberOfCells();
    for (int i = 1; i <= sr; i++) {
      Row sheetrow = sheet.getRow(i);
      Cell cell = sheetrow.getCell(lastcellnum - 1);
      Cell tablename = sheetrow.getCell(0);
      if (cell.getNumericCellValue() > 5) {
        lesserDataTable.add(tablename.toString());
      } else if (cell.getNumericCellValue() < -5) {
        biggerDataTable.add(tablename.toString());
      } else if (cell.getNumericCellValue() == 0) {
        sameSizeDataTable.add(tablename.toString());
      }
    }
  }

  public void createSizeColumn(String filelocation) {
    try {
      String prefix = getPrefix();
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Retrieve the row and check for null
      // int lastrow = sheet.getLastRowNum();
      // Update the value of cell
      Row sheetrow = sheet.getRow(0);
      int lastcell = sheetrow.getPhysicalNumberOfCells();
      Cell cell = sheetrow.getCell(lastcell);
      if (cell == null) {
        cell = sheetrow.createCell(lastcell);
      }
      cell.setCellValue("Size in Mbytes till date : " + prefix);
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("Size column has been crreated successfully");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void createNumberofRowsColumn(String filelocation) {
    try {
      String prefix = getPrefix();
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Update the value of cell
      Row sheetrow = sheet.getRow(0);
      int lastcell = sheetrow.getPhysicalNumberOfCells();
      Cell cell = sheetrow.getCell(lastcell);
      if (cell == null) {
        cell = sheetrow.createCell(lastcell);
      }
      cell.setCellValue("Total number of rows till date : " + prefix);
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("Total-number-of-rows column has been crreated successfully");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
