package com.mediaiq.dm.helpers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ApachePOIExcelReadDCM {

  public static List<String> tablenames = new ArrayList();
  public static List<String> tablesizes = new ArrayList<>();

  public List<String> getTableName(String filelocation) throws IOException {
    String tablename = null;
    FileInputStream excelFile = new FileInputStream(new File(filelocation));
    Workbook workbook = new XSSFWorkbook(excelFile);
    Sheet datatypeSheet = workbook.getSheetAt(0);
    int rowCount = datatypeSheet.getLastRowNum() - datatypeSheet.getFirstRowNum();
    for (int i = 1; i < rowCount + 1; i++) {
      Row row = datatypeSheet.getRow(i);
      tablename = row.getCell(0).getStringCellValue();
      tablenames.add(tablename);
    }
    return tablenames;
  }

  public void writeXLSXFile(String filelocation, List<String> tables)
      throws IOException, ClassNotFoundException, SQLException, InterruptedException {
    try {
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Retrieve the row and check for null
      int lastrow = sheet.getLastRowNum();
      // Update the value of cell
      for (int i = 1; i <= lastrow; i++) {
        Row sheetrow = sheet.getRow(i);
        int lastcell = sheetrow.getPhysicalNumberOfCells();
        Cell cell = sheetrow.getCell(lastcell);
        if (cell == null) {
          cell = sheetrow.createCell(lastcell);
        }
        cell.setCellValue(tables.get(i - 1));
      }
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("The file writing is finished");

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void createSizeColumn(String filelocation) {
    try {
      String prefix = getPrefix();
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Retrieve the row and check for null
      // int lastrow = sheet.getLastRowNum();
      // Update the value of cell
      Row sheetrow = sheet.getRow(0);
      int lastcell = sheetrow.getPhysicalNumberOfCells();
      Cell cell = sheetrow.getCell(lastcell);
      if (cell == null) {
        cell = sheetrow.createCell(lastcell);
      }
      cell.setCellValue("Size in Mbytes till date : " + prefix);
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("Size column has been crreated successfully");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void createNumberofRowsColumn(String filelocation) {
    try {
      String prefix = getPrefix();
      FileInputStream file = new FileInputStream(new File(filelocation));
      Workbook workbook = new XSSFWorkbook(file);
      Sheet sheet = workbook.getSheetAt(0);
      // Update the value of cell
      Row sheetrow = sheet.getRow(0);
      int lastcell = sheetrow.getPhysicalNumberOfCells();
      Cell cell = sheetrow.getCell(lastcell);
      if (cell == null) {
        cell = sheetrow.createCell(lastcell);
      }
      cell.setCellValue("Total number of rows till date : " + prefix);
      file.close();
      FileOutputStream outFile = new FileOutputStream(new File(filelocation));
      workbook.write(outFile);
      outFile.close();
      System.out.println("Total-number-of-rows column has been crreated successfully");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String getPrefix() {
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Date date = new Date();
    SimpleDateFormat mf = new SimpleDateFormat("MM");
    SimpleDateFormat df = new SimpleDateFormat("dd");
    SimpleDateFormat hr = new SimpleDateFormat("HH");
    SimpleDateFormat mm = new SimpleDateFormat("MM");
    String month = mf.format(date);
    String day = df.format(date);
    String hour = hr.format(date);
    String minute = mm.format(date);
    String prefix = year + "/" + month + "/" + day + " " + hour + ":" + minute;
    return prefix;
  }

  public static int getRowCount(String filename) throws IOException {
    InputStream is = new BufferedInputStream(new FileInputStream(filename));
    try {
      byte[] c = new byte[1024];
      int count = 0;
      int readChars = 0;
      boolean empty = true;
      while ((readChars = is.read(c)) != -1) {
        empty = false;
        for (int i = 0; i < readChars; ++i) {
          if (c[i] == '\n') {
            ++count;
          }
        }
      }
      return (count == 0 && !empty) ? 1 : count;
    } finally {
      is.close();
    }
  }
}
