package com.mediaiq.dm.helpers;

import java.io.IOException;

public class CommandLineUtils {
  public void executeCommand(String command) throws IOException, InterruptedException {
    Process pb = Runtime.getRuntime().exec(new String[] { "/bin/bash", "-c", command });
    pb.waitFor();
  }
}
