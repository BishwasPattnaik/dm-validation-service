package com.mediaiq.dm.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateHtmlpage {
  public static String createReportHtml(String consolefilepath, String s3filepath)
      throws IOException {
    File file = new File(System.getProperty("user.dir") + "/report.html");
    FileWriter fileWriter = null;
    BufferedWriter bufferedWriter = null;
    fileWriter = new FileWriter(file);
    bufferedWriter = new BufferedWriter(fileWriter);
    String htmlPage = "<!DOCTYPE html><html><head><style>tr:nth-child(even){background-color: #383838},table, th, td { border: 1px red;}</style></head><body><font color=#9D9DAC><table style='width:80%;background-color:#151415;'> <tr> <th>AdvertiserID</th> <th>S3Count</th> <th>ConsoleCount</th> <th>Difference</th </tr>"
        + DCMProcessing.generateHtmlTag(consolefilepath, s3filepath) + "</table></body></html>";
    bufferedWriter.write(htmlPage);
    System.out.println("Html page created");
    bufferedWriter.flush();
    fileWriter.flush();
    return htmlPage;
  }
}
