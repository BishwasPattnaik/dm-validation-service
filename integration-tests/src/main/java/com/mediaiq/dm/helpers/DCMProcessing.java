package com.mediaiq.dm.helpers;

import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.opencsv.CSVReader;

public class DCMProcessing {
  public static CommandLineUtils commonutilsobj = new CommandLineUtils();
  public static List<Integer> count = new ArrayList<>();
  public static List<Integer> advID = new ArrayList<>();
  public static List<String> value = new ArrayList<>();
  public static List<Integer> profileId = new ArrayList<>();
  public static List<Integer> reportId = new ArrayList<>();
  public static List<Integer> tatalrows = new ArrayList<>();
  public static List<Integer> profileIDCount = new ArrayList<>();
  public static String dcmpre = null;
  public static String profilekey = null;
  public static String filename = null;
  public static String reportdate = null;
  public static List<String> wholeFile = new ArrayList<>();
  public static List<String> columnCount = new ArrayList<>();
  public static List<Integer> reports = new ArrayList<>();
  public static List<String> dcmFileNames = new ArrayList<>();
  public static HashMap<Integer, Integer> dcmadvidwithcount = new HashMap<>();
  public static HashMap<Integer, Integer> s3advidwithcount = new HashMap<>();

  public String getWholeFile(String file) throws IOException {
    FileReader filereader = new FileReader(file);
    CSVReader csvReader = new CSVReader(filereader);
    String[] nextRecord;
    String val = null;
    while ((nextRecord = csvReader.readNext()) != null) {
      for (String cell : nextRecord) {
        wholeFile.add(cell);
        val = cell;
        value.add(val);
      }
    }
    return val;
  }

  public static String generateHtmlTag(String consolefilepath, String s3filepath)
      throws IOException {
    FileHandlingUtils.getAdvertiserCount(consolefilepath, s3filepath);
    List<String> details = new ArrayList<>();
    for (int i = 0; i < FileHandlingUtils.consoleID.size(); i++) {
      String adv = "<td>" + String.valueOf(FileHandlingUtils.consoleID.get(i)) + "</td>";
      String s3 = "<td>" + String.valueOf(FileHandlingUtils.s3count.get(i)) + "</td>";
      String console = "<td>" + String.valueOf(FileHandlingUtils.consolecount.get(i)) + "</td>";
      if (FileHandlingUtils.getDifference().get(i) == 0) {
        String diff = "<td>" + String.valueOf(FileHandlingUtils.getDifference().get(i)) + "</td>";
        details.add("<tr align='center'>" + adv + s3 + console + diff + "</tr>");
      } else {
        String diff = "<td>" + String.valueOf(FileHandlingUtils.getDifference().get(i)) + "</td>";
        details.add("<tr style='color:red' align='center'>" + adv + s3 + console + diff + "</tr>");
      }
    }
    return String.join("", details);
  }

  public static void runValidationDCMConsole(String profile) throws ClassNotFoundException,
      SQLException, IOException, InterruptedException, ParseException {
    ApachePOIExcelReadDCM apacheobj = new ApachePOIExcelReadDCM();
    CommandLineUtils commonutilsobj = new CommandLineUtils();
    JdbcUtilFeed jdbcobj = new JdbcUtilFeed();
    S3ConnectorDCM s3obj = new S3ConnectorDCM();
    s3obj.getPrefix();
    DCMProcessing procobj = new DCMProcessing();
    FileHandlingUtils fileobj = new FileHandlingUtils();
    String reportdate = s3obj.getPastDayDCM(2);
    String token = HttpUtils.getAccesstoken(profile);
    jdbcobj.getRecord("select * from dcm_profiles_" + profile + "_feed ;");
    profileId = jdbcobj.profileId;
    reportId = jdbcobj.reportId;
    int totalrowCount = 0;
    for (int i = 0; i < profileId.size(); i++) {
      String filename = reportId.get(i) + ".csv";
      String downloadedfilename = "recentReport.csv";
      // Getting the API url for report download
      String apiurl = HttpUtils
          .getapiURLPast("https://www.googleapis.com/dfareporting/v3.1/userprofiles/"
              + profileId.get(i) + "/files", token);
      if (apiurl != null) {
        HttpUtils.downloadFile(apiurl, reportId.get(i), token);
        // Getting only the reports of a specific date
        commonutilsobj.executeCommand(
            "cat " + filename + " | grep " + reportdate + " > " + downloadedfilename);
        totalrowCount = ApachePOIExcelReadDCM
            .getRowCount(System.getProperty("user.dir") + "/" + downloadedfilename);
        if (totalrowCount != 0) {
          // Getting the total number of columns
          commonutilsobj.executeCommand(
              "head -1 " + downloadedfilename + " | sed 's/[^,]//g' | wc -c > totalrows");
          columnCount.add(procobj.getWholeFile("totalrows"));
          int columncount = Integer.parseInt(columnCount.get(0));
          if (columncount == 19) {
            commonutilsobj.executeCommand("cat " + downloadedfilename + " >> merged.csv");
            commonutilsobj.executeCommand(
                "echo " + profileId.get(i) + " " + totalrowCount + " >> profileIdWithCount.csv");
            // Getting advertiser-id's count from each file and redirecting to mergedfile
            commonutilsobj.executeCommand(
                "awk -F ',' '{print $1}' " + downloadedfilename + " | uniq -c >> mergedfile.csv");
            reports.add(reportId.get(i));
          }
          profileIDCount.add(totalrowCount);
        } else {
          profileIDCount.add(0);
        }
      }
      // FileHandlingUtils.deleteFiles(downloadedfilename);
      FileHandlingUtils.deleteFiles(filename);
      FileHandlingUtils.deleteFiles("totalrows");
    }

    // Getting uniq advertiser-id count from merged file
    commonutilsobj.executeCommand("sort mergedfile.csv | uniq > DCMAdvCount.csv");
    // Making a sum of all the counts
    dcmadvidwithcount = FileHandlingUtils.getAdvIdWithCount("DCMAdvCount.csv");
    System.out.println("Profile-Ids : " + profileId.toString());
    System.out.println("Profile-Ids : " + profileIDCount.toString());
    for (int i = 0; i < profileId.size(); i++) {
      System.out.println(profileId.get(i) + " : " + profileIDCount.get(i));
    }
    MailUtil.sendEmail("bishwas@miqdigital.com", "DCM Automation Report", "**************"
        + ApachePOIExcelReadDCM.getRowCount(System.getProperty("user.dir") + "/" + "merged.csv"));
  }

  public static void runValidationS3(String profile)
      throws IOException, ParseException, InterruptedException {
    S3ConnectorDCM s3obj = new S3ConnectorDCM();
    S3ConnectorDCM.getPastDay(1);
    s3obj.getDCMfileName(s3obj.dcmpreyday);
    if (profile == "miq") {
      profilekey = s3obj.miqprofilekey14thHour;
      filename = s3obj.miqfilename14thHour;
    } else {
      profilekey = s3obj.naprofilekey14thHour;
      filename = s3obj.nafilename14thHour;
    }
    int totalrowCount = 0;
    s3obj.downloadFileFromS("aiqdatabucket", profilekey, filename);
    String convertedFileName = filename.substring(0, 39);
    commonutilsobj.executeCommand("gunzip " + convertedFileName);
    // Getting unique counts of each advertiser
    commonutilsobj.executeCommand(
        "awk '{print $1}'  " + filename.substring(0, 39) + " | uniq -c > AdvertiserIdCountS3.csv");
    s3advidwithcount = FileHandlingUtils.getAdvIdWithCount("AdvertiserIdCountS3.csv");
    totalrowCount = ApachePOIExcelReadDCM
        .getRowCount(System.getProperty("user.dir") + "/" + convertedFileName);
    MailUtil.sendEmail("bishwas@miqdigital.com", "DCM Automation Report",
        "###########" + totalrowCount);
  }
}
