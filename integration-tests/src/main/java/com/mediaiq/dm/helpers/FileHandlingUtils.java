package com.mediaiq.dm.helpers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mongodb.client.model.ReturnDocument;

public class FileHandlingUtils {
  public static List<Integer> consolecount = new ArrayList<>();
  public static List<Integer> s3count = new ArrayList<>();
  public static List<Integer> consoleID = new ArrayList<>();
  public static int totalcount;

  public static void deleteFiles(String filename) {
    File file = new File(filename);
    if (file.delete()) {
      System.out.println("File deleted successfully");
    } else {
      System.out.println("Failed to delete the file");
    }
  }

  public static HashMap<Integer, Integer> getAdvIdWithCount(String filepath) throws IOException {
    HashMap<Integer, Integer> advidwithcount = new HashMap<>();
    File file = new File(filepath);
    List<String> files = new ArrayList<>();
    BufferedReader br = new BufferedReader(new FileReader(file));
    String st;
    while ((st = br.readLine()) != null) {
      files.add(st);
    }
    for (int i = 0; i < files.size(); i++) {
      String[] ele = files.get(i).split("\\s+");
      for (int j = 0; j < ele.length; j++) {
        advidwithcount.put(Integer.parseInt(ele[2]), Integer.parseInt(ele[1]));
      }
    }
    return advidwithcount;
  }

  public static List<String> getFileDetails(String consolefilepath) throws IOException {
    File file1 = new File(consolefilepath);
    List<String> files = new ArrayList<>();
    BufferedReader br = new BufferedReader(new FileReader(file1));
    String st;
    while ((st = br.readLine()) != null) {
      files.add(st);
    }
    return files;
  }

  public static List<Integer> getDifference() throws IOException {
    List<Integer> difference = new ArrayList<>();
    int diff = 0;
    for (int i = 0; i < s3count.size(); i++) {
      diff = consolecount.get(i) - s3count.get(i);
      difference.add(diff);
    }
    return difference;
  }

  public static void getAdvertiserCount(String consolefilepath, String s3filepath)
      throws IOException {
    List<String> consoledetails = getFileDetails(consolefilepath);
    for (int i = 0; i < consoledetails.size(); i++) {
      String[] ele = consoledetails.get(i).split("\\s+");
      consolecount.add(Integer.parseInt(ele[1]));
      consoleID.add(Integer.parseInt(ele[2]));
    }
    HashMap<Integer, Integer> idWithCount = new HashMap<>();
    idWithCount = getAdvIdWithCount(s3filepath);
    for (int i = 0; i < consoleID.size(); i++) {
      s3count.add(idWithCount.get(consoleID.get(i)));
    }
  }

  public static Integer getTotalDcmCount() {
    int dcmsum = 0;
    for (int i = 0; i < consolecount.size(); i++) {
      dcmsum = dcmsum + consolecount.get(i);
    }
    return dcmsum;
  }

  public static Integer getTotalS3Count() {
    int s3sum = 0;
    for (int i = 0; i < s3count.size(); i++) {
      s3sum = s3sum + s3count.get(i);
    }
    return s3sum;
  }
}
