package com.mediaiq.dm.helpers;

import com.google.gson.JsonObject;
import io.restassured.response.Response;
import java.io.File;

public class Helpers {



  public static int receivedstatus;
  public static int pipelinereceivedstatus;
  public static int updatereceivedstatus;
  public static String id;
  public static final String auth =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MDQyNDY0MDksImV4cCI6MTUzNTc4MjQwOSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImF1dGhvcml0aWVzIjpbIkFNQVBfREVWIiwiQUlRWF9DTElFTlQiLCJERVYiLCJBSVFYX0NMSUVOVF9BRE1JTiIsIkRBVEFfTUFOQUdFTUVOVCJdfQ.m-mhvugb_0M-vAEuaqgSHojAk8KOVncRTQySulZGwHM";
  public static final File s3 =
      new File("src/test/resources/testData/S3connectionApiPostBody.json");
  public static final File sftp =
      new File("src/test/resources/testData/SftpconnectionApiPostBody.json");
  public static final File s3_to_sftp_template =
      new File("src/test/resources/testData/SftpToS3Template.json");
  public static final File pipeline_create_body =
      new File("src/test/resources/testData/pipelineApiPostBody.json");
  public static Response r;
  public static String routeres;
  public static final File routeresponebody =
      new File("src/test/resources/testData/route-response.json");
  public static JsonObject responseObject;
  public static String urlWithId;

}
