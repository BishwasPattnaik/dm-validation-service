package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import groovy.json.StringEscapeUtils;

public class HttpUtils {
  public static List<Integer> profileId = new ArrayList<>();
  public static List<Integer> reportId = new ArrayList<>();
  public static List<String> fileNames = new ArrayList<>();

  public static void downloadFile(String apiUrl, int reportID, String token)
      throws ClientProtocolException, IOException, ParseException, InterruptedException {
    HttpClient client = new DefaultHttpClient();
    HttpGet request = new HttpGet(apiUrl);
    request.addHeader("Authorization", " Bearer " + token);
    HttpResponse response = client.execute(request);
    HttpEntity resEntity = response.getEntity();
    InputStream instream = resEntity.getContent();
    OutputStream outputStream = new FileOutputStream(
        new File(System.getProperty("user.dir") + "/" + reportID + ".csv"));
    Thread.sleep(2000);
    int read = 0;
    byte[] bytes = new byte[1024];

    while ((read = instream.read(bytes)) != -1) {
      outputStream.write(bytes, 0, read);
    }
    System.out.println("File download is Done!");
    request.abort();
    request.releaseConnection();
  }

  public String getAccesstokenNA() throws ClientProtocolException, IOException, ParseException {
    HttpClient client = new DefaultHttpClient();
    HttpPost request = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
    request.setHeader("Content-Type", "application/x-www-form-urlencoded");
    String body = "grant_type=refresh_token&client_id=428941784056-714den17g2a5jatpmct1ohgs1mc4il0b.apps.googleusercontent.com&client_secret=Jog4MqpauFod2RKV3bDzmvuH&refresh_token=1%2FFnno_uyHuKpd5WsfqHNm4VN7WLqzvSdsSXw1_KntgPw";
    StringEntity entity = new StringEntity(body);
    request.setEntity(entity);
    HttpResponse response = client.execute(request);
    ResponseHandler<String> handler = new BasicResponseHandler();
    String responsebody = handler.handleResponse(response);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responsebody);
    return (String) jsonObject.get("access_token");
  }

  public static String getAccesstoken(String profile)
      throws ClientProtocolException, IOException, ParseException {
    String body = "";
    HttpClient client = new DefaultHttpClient();
    HttpPost request = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
    request.setHeader("Content-Type", "application/x-www-form-urlencoded");
    if (profile == "na") {
      body = "grant_type=refresh_token&client_id=428941784056-714den17g2a5jatpmct1ohgs1mc4il0b.apps.googleusercontent.com&client_secret=Jog4MqpauFod2RKV3bDzmvuH&refresh_token=1%2FFnno_uyHuKpd5WsfqHNm4VN7WLqzvSdsSXw1_KntgPw";
    } else {
      body = "grant_type=refresh_token&client_id=1046831607667-pgjmdr6fi3vdsju8o8dq4ccp6vtj76mr.apps.googleusercontent.com&client_secret=z6TXn87DWOVOZedzpVvGdDAR&refresh_token=1/iCa_XsXzikH413de7ejekuNjRgpEQasa2QfFpP3jyUI";
    }
    StringEntity entity = new StringEntity(body);
    request.setEntity(entity);
    HttpResponse response = client.execute(request);
    ResponseHandler<String> handler = new BasicResponseHandler();
    String responsebody = handler.handleResponse(response);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responsebody);
    return (String) jsonObject.get("access_token");
  }

  public String getAccesstokenMIQ() throws ClientProtocolException, IOException, ParseException {
    HttpClient client = new DefaultHttpClient();
    HttpPost request = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
    request.setHeader("Content-Type", "application/x-www-form-urlencoded");
    String body = "grant_type=refresh_token&client_id=1046831607667-pgjmdr6fi3vdsju8o8dq4ccp6vtj76mr.apps.googleusercontent.com&client_secret=z6TXn87DWOVOZedzpVvGdDAR&refresh_token=1/iCa_XsXzikH413de7ejekuNjRgpEQasa2QfFpP3jyUI";
    StringEntity entity = new StringEntity(body);
    request.setEntity(entity);
    HttpResponse response = client.execute(request);
    ResponseHandler<String> handler = new BasicResponseHandler();
    String responsebody = handler.handleResponse(response);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responsebody);
    return (String) jsonObject.get("access_token");
  }

  public String getapiURL(String apiUrl, String accesstoken)
      throws ClientProtocolException, IOException, ParseException {
    String apiURL = null;
    HttpClient client = new DefaultHttpClient();
    HttpGet request = new HttpGet(apiUrl);
    request.addHeader("Authorization", " Bearer " + accesstoken);
    HttpResponse response = client.execute(request);
    ResponseHandler<String> handler = new BasicResponseHandler();
    String responsebody = handler.handleResponse(response);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responsebody);
    JSONArray items = (JSONArray) jsonObject.get("items");
    for (int i = 0; i < items.size(); i++) {
      JSONObject itemselements = (JSONObject) items.get(i);
      JSONObject dateRange = (JSONObject) itemselements.get("dateRange");
      if (itemselements.get("status").equals("REPORT_AVAILABLE")
          && dateRange.get("endDate").equals(getPastDay(1))
          && dateRange.get("startDate").equals(getPastDay(7))) {
        JSONObject urls = (JSONObject) itemselements.get("urls");
        apiURL = StringEscapeUtils.unescapeJava(urls.get("apiUrl").toString());
        break;
      } else {
        String fileName = (String) itemselements.get("fileName");
        String fileId = (String) itemselements.get("id");
        System.out.println("No reports available for the fileName : " + fileName
            + " and for reportId : " + fileId);
        apiURL = null;
      }
    }
    return apiURL;
  }

  public static String getapiURLPast(String apiUrl, String accesstoken)
      throws ClientProtocolException, IOException, ParseException {
    String apiURL = null;
    HttpClient client = new DefaultHttpClient();
    HttpGet request = new HttpGet(apiUrl);
    request.addHeader("Authorization", " Bearer " + accesstoken);
    HttpResponse response = client.execute(request);
    ResponseHandler<String> handler = new BasicResponseHandler();
    String responsebody = handler.handleResponse(response);
    JSONParser jsonParser = new JSONParser();
    JSONObject jsonObject = (JSONObject) jsonParser.parse(responsebody);
    JSONArray items = (JSONArray) jsonObject.get("items");
    for (int i = 0; i < items.size(); i++) {
      JSONObject itemselements = (JSONObject) items.get(i);
      JSONObject dateRange = (JSONObject) itemselements.get("dateRange");
      if (itemselements.get("status").equals("REPORT_AVAILABLE")
          && dateRange.get("endDate").equals(getPastDay(2))
          && dateRange.get("startDate").equals(getPastDay(8))) {
        JSONObject urls = (JSONObject) itemselements.get("urls");
        apiURL = StringEscapeUtils.unescapeJava(urls.get("apiUrl").toString());
        break;
      } else {
        String fileName = (String) itemselements.get("fileName");
        String fileId = (String) itemselements.get("id");
        System.out.println("No reports available for the fileName : " + fileName
            + " and for reportId : " + fileId);
        apiURL = null;
      }
    }
    return apiURL;
  }

  public static String getPastDay(int desiredDate) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -desiredDate);
    String Day = dateFormat.format(calendar.getTime());
    return Day;
  }

}
