package com.mediaiq.dm.helpers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JdbcUtil {
  static final String redshiftUrl = "jdbc:redshift://miq-warehouse-2.ciactbellnsn.us-east-1.redshift.amazonaws.com:5439/warehouse";
  static final String masterUsername = "datapipeline";
  static final String password = "Data190Pline_9";
  private static Connection connection = null;
  private static Statement statement = null;
  public static List<String> tablenames = new ArrayList<>();
  public static List<Double> tablesizes = new ArrayList<>();
  public static List<Double> totalrows = new ArrayList<>();

  public JdbcUtil(String filename) throws IOException {
    ApachePOIExcelRead obj = new ApachePOIExcelRead();
    tablenames = obj.getTableName(filename);
    System.out.println("The test execution has been started");
  }

  public void getTableSize(String schema, String columnname, String otherdata)
      throws ClassNotFoundException, SQLException, IOException, InterruptedException {
    ApachePOIExcelRead obj = new ApachePOIExcelRead();
    System.out.println("The redshift query has started");
    try {
      for (String a : tablenames) {
        Class.forName("com.amazon.redshift.jdbc42.Driver");
        Properties properties = new Properties();
        properties.setProperty("user", masterUsername);
        properties.setProperty("password", password);
        connection = DriverManager.getConnection(redshiftUrl, properties);
        statement = connection.createStatement();
        String query = "select trim(pgdb.datname) as Database,trim(pgn.nspname) as Schema,trim(a.name) as Table,b.mbytes,a.rows from (select db_id, id, name, sum(rows) as rows from stv_tbl_perm a group by db_id, id, name) as a join pg_class as pgc on pgc.oid = a.id  join pg_namespace as pgn on pgn.oid = pgc.relnamespace join pg_database as pgdb on pgdb.oid = a.db_id join (select tbl, count(*) as mbytes from stv_blocklist group by tbl) b on a.id = b.tbl where pgn.nspname ilike '%"
            + schema + "%' and a.name = " + "'" + a.toLowerCase() + "'";
        ResultSet resultSet = statement.executeQuery(query);
        // Further code to follow
        if (resultSet.next()) {
          String tn = resultSet.getString(columnname);
          double inttn = (Double.parseDouble(tn.toString()))/1000;
          String ts = resultSet.getString(otherdata);
          double intts = Double.parseDouble(ts.toString());
          tablesizes.add(inttn);
          totalrows.add(intts);
        } else {
          tablesizes.add((double) 0);
          totalrows.add((double) 0);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    connection.close();
    System.out
        .println("The redshift query is finished and the values are being added in the list.");
  }

}