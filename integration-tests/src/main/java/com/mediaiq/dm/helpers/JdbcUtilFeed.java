package com.mediaiq.dm.helpers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JdbcUtilFeed {
  static final String redshiftUrl = "jdbc:redshift://miq-warehouse-2.ciactbellnsn.us-east-1.redshift.amazonaws.com:5439/warehouse";
  static final String masterUsername = "datapipeline";
  static final String password = "Data190Pline_9";
  private static Connection connection = null;
  private static Statement statement = null;
  public static List<String> tablesizes = new ArrayList<>();
  public static List<String> totalrows = new ArrayList<>();
  public static List<String> tablenames = new ArrayList<>();
  private static String FILE_NAME = System.getProperty("user.dir")
      + "/src/test/resources/Report.xlsx";
  public static List<Integer> profileId = new ArrayList<>();
  public static List<Integer> reportId = new ArrayList<>();

  public void getTableSize(String filename, String schema, String columnname, String otherdata)
      throws ClassNotFoundException, SQLException, IOException, InterruptedException {
    ApachePOIExcelRead obj = new ApachePOIExcelRead();
    tablenames = obj.getTableName(filename);
    try {
      for (String a : tablenames) {
        Class.forName("com.amazon.redshift.jdbc42.Driver");
        Properties properties = new Properties();
        properties.setProperty("user", masterUsername);
        properties.setProperty("password", password);
        connection = DriverManager.getConnection(redshiftUrl, properties);
        statement = connection.createStatement();
        String query = "select trim(pgdb.datname) as Database,trim(pgn.nspname) as Schema,trim(a.name) as Table,b.mbytes,a.rows from (select db_id, id, name, sum(rows) as rows from stv_tbl_perm a group by db_id, id, name) as a join pg_class as pgc on pgc.oid = a.id  join pg_namespace as pgn on pgn.oid = pgc.relnamespace join pg_database as pgdb on pgdb.oid = a.db_id join (select tbl, count(*) as mbytes from stv_blocklist group by tbl) b on a.id = b.tbl where pgn.nspname ilike '%"
            + schema + "%' and a.name = " + "'" + a.toLowerCase() + "'";
        ResultSet resultSet = statement.executeQuery(query);
        // Further code to follow
        if (resultSet.next()) {
          String tn = resultSet.getString(columnname);
          String ts = resultSet.getString(otherdata);
          tablesizes.add(tn);
          totalrows.add(ts);
        } else {
          tablesizes.add("Null");
          totalrows.add("Null");
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    connection.close();
    System.out.println("All values are being added in a list");
  }

  public static void getRecord(String query)
      throws ClassNotFoundException, SQLException, IOException, InterruptedException {
    try {
      Class.forName("com.amazon.redshift.jdbc42.Driver");
      Properties properties = new Properties();
      properties.setProperty("user", masterUsername);
      properties.setProperty("password", password);
      connection = DriverManager.getConnection(redshiftUrl, properties);
      statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);
      // Further code to follow
      while (resultSet.next()) {
        int pId = resultSet.getInt(1);
        int rId = resultSet.getInt(3);
        profileId.add(pId);
        reportId.add(rId);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    connection.close();
    System.out.println("All values are being added in a list");
  }
  public static void main(String[] args) {
    
  }
}