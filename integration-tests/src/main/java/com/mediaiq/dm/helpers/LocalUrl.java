package com.mediaiq.dm.helpers;

public class LocalUrl {
  public static final String sdc = "http://localhost:8003/v0.2.2/sdc";
  public static final String connection = "http://localhost:8003/v0.2.2/connection/";
  public static final String connections = "http://localhost:8003/v0.2.2/connections/";
  public static final String route = "http://localhost:8003/v0.2.2";
  public static final String template = "http://localhost:8003/v0.2.2/template/";
  public static final String pipeline = "http://localhost:8003/v0.2.2/pipeline/";
  public static final String pipelines = "http://localhost:8003/v0.2.2/pipelines";
}
