package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.simple.parser.ParseException;

/**
 * Mail Util with SSL Authentication
 */

public class MailUtil {

  // Sender's email address.
  private static final String EMAIL = "feed@miqdigital.com";

  // Sender's app password.
  private static final String PASSWORD = "jpfidbbufijyqurn";
  private static Properties mailProps;
  private static Authenticator auth;

  static {
    mailProps = new Properties();
    mailProps.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
    mailProps.put("mail.smtp.socketFactory.port", "465"); // SSL Port
    mailProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // SSL Factory
                                                                                      // Class
    mailProps.put("mail.smtp.auth", "true"); // Enabling SMTP Authentication
    mailProps.put("mail.smtp.port", "465"); // SMTP Port

    auth = new Authenticator() {
      // override the getPasswordAuthentication method
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(EMAIL, PASSWORD);
      }
    };
  }

  /**
   * A util method to sent an email.
   * 
   * @param toEmail
   *          is the recipient's email address.
   * @param subject
   *          is the subject of an email.
   * @param body
   *          is the body of an email.
   * @throws MessagingException
   * @throws UnsupportedEncodingException
   * @throws ParseException
   * @throws IOException
   */
  public static void sendEmailHtml(String toEmail, String toEmail2, String subject, String header,
      String body, String htmlBody) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress("no_reply@example.com", header));
      msg.setSubject(subject, "UTF-8");
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      // set message headers
      Multipart multipart = new MimeMultipart();
      // PLAIN TEXT
      BodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setText(body);
      multipart.addBodyPart(messageBodyPart);
      // HTML TEXT
      messageBodyPart = new MimeBodyPart();
      messageBodyPart.setContent(htmlBody, "text/html");
      multipart.addBodyPart(messageBodyPart);

      msg.setContent(multipart);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void sendEmail(String toEmail, String subject, String body) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session); // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", "Feed Details Report"));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void sendEmailDCM(String toEmail1, String toEmail2, String subject, String header,
      String body) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", header));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail1, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      // Adding a body part
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void sendEmailDCM(String toEmail1, String toEmail2, String subject, String header,
      String body, String filepath, String filepath2) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", header));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body, "Content-Type", "text/html");
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail1, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      // Adding a body part
      Multipart multipart = new MimeMultipart();
      MimeBodyPart attachPart = new MimeBodyPart();
      DataSource source = new FileDataSource(filepath);
      attachPart.setDataHandler(new DataHandler(source));
      attachPart.setFileName(new File(filepath).getName());
      attachPart.setHeader("Content-Type", "text/html");
      multipart.addBodyPart(attachPart);
      //
      MimeBodyPart attachPart2 = new MimeBodyPart();
      DataSource source2 = new FileDataSource(filepath2);
      attachPart2.setDataHandler(new DataHandler(source2));
      attachPart2.setFileName(new File(filepath2).getName());
      multipart.addBodyPart(attachPart2);
      msg.setContent(multipart);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendEmailDCMNoFile(String toEmail1, String toEmail2, String subject, String header,
      String body) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", subject));
      msg.setSubject(header, "UTF-8");
      msg.setText(body, "UTF-8");
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail1, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      // sets the multipart as message's content
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void sendEmailFeed(String toEmail, String subject, String body, String filepath) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", "Feed Details Report"));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      // Adding a body part
      Multipart multipart = new MimeMultipart();
      MimeBodyPart attachPart = new MimeBodyPart();
      DataSource source = new FileDataSource(filepath);
      attachPart.setDataHandler(new DataHandler(source));
      attachPart.setFileName(new File(filepath).getName());

      multipart.addBodyPart(attachPart);
      // sets the multipart as message's contenthttps://mail.google.com/mail/u/0/#inbox
      msg.setContent(multipart);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}