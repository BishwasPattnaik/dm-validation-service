package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.simple.parser.ParseException;

/**
 * Mail Util with SSL Authentication
 */

public class MailUtilCorrupt {

  // Sender's email address.
  private static final String EMAIL = "feed@miqdigital.com";

  // Sender's app password.
  private static final String PASSWORD = "jpfidbbufijyqurn";
  private static Properties mailProps;
  private static Authenticator auth;

  static {
    mailProps = new Properties();
    mailProps.put("mail.smtp.host", "smtp.gmail.com");
    mailProps.put("mail.smtp.socketFactory.port", "465");
    mailProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

    mailProps.put("mail.smtp.auth", "true");
    mailProps.put("mail.smtp.port", "465");

    auth = new Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(EMAIL, PASSWORD);
      }
    };
  }

  /**
   * A util method to sent an email.
   * 
   * @param toEmail
   *          is the recipient's email address.
   * @param subject
   *          is the subject of an email.
   * @param body
   *          is the body of an email.
   * @throws ParseException
   * @throws IOException
   */
  public void sendEmailDCM(String toEmail, String toEmail2, String subject, String body) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", subject));
      msg.setSubject("DCM File-Size Details", "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendEmail(String toEmail, String subject, String body) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", subject));
      msg.setSubject("Table details on basis of size difference", "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendEmail(String toEmail, String subject, String body, String filepath) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", "Feed Details Report"));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      // Adding a body part
      Multipart multipart = new MimeMultipart();
      MimeBodyPart attachPart = new MimeBodyPart();
      DataSource source = new FileDataSource(filepath);
      attachPart.setDataHandler(new DataHandler(source));
      attachPart.setFileName(new File(filepath).getName());

      multipart.addBodyPart(attachPart);
      msg.setContent(multipart);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendEmail(String toEmail, String toEmail2, String subject, String body,
      String filepath) {
    try {
      Session session = Session.getDefaultInstance(mailProps, auth);
      MimeMessage msg = new MimeMessage(session);
      // set message headers
      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
      msg.addHeader("format", "flowed");
      msg.addHeader("Content-Transfer-Encoding", "8bit");
      msg.setFrom(new InternetAddress("no_reply@example.com", "Feed Details Report"));
      msg.setSubject(subject, "UTF-8");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
      msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(toEmail2, false));
      // Adding a body part
      Multipart multipart = new MimeMultipart();
      MimeBodyPart attachPart = new MimeBodyPart();
      DataSource source = new FileDataSource(filepath);
      attachPart.setDataHandler(new DataHandler(source));
      attachPart.setFileName(new File(filepath).getName());

      multipart.addBodyPart(attachPart);
      msg.setContent(multipart);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}