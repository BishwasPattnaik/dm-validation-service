package com.mediaiq.dm.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MongoConnector {
  public MongoClient client;
  public DBCollection collection;

  public MongoConnector(String env, String server_url, int server_port, String username,
      String dbname, String password) {
    if (env == "staging" || env == "qa") {
      List<ServerAddress> seeds = new ArrayList<>();
      seeds.add(new ServerAddress(server_url, server_port));
      List<MongoCredential> credentials = new ArrayList<>();
      credentials
          .add(MongoCredential.createScramSha1Credential(username, dbname, password.toCharArray()));
      client = new MongoClient(seeds, credentials);
    } else if (env == "local") {
      client = new MongoClient(server_url, server_port);
    }
  }

  public MongoConnector(String env, String server_url, int server_port, String username,
      String dbname, String password, String collectionname) {
    if (env == "staging" || env == "qa") {
      List<ServerAddress> seeds = new ArrayList<>();
      seeds.add(new ServerAddress(server_url, server_port));
      List<MongoCredential> credentials = new ArrayList<>();
      credentials
          .add(MongoCredential.createScramSha1Credential(username, dbname, password.toCharArray()));
      client = new MongoClient(seeds, credentials);
    } else if (env == "local") {
      client = new MongoClient(server_url, server_port);
    }
    collection = client.getDB(dbname).getCollection(collectionname);
  }

  /*
   * Provides the template id by type
   * SFTP_PASSWORD_TO_S3_PASSWORD_BATCH/HTTP_APIKEY_TO_S3_PASSWORD_BATCH/
   * HTTP_TOKEN_TO_S3_PASSWORD_BATCH
   */
  public List<DBObject> getDocumentDetails(String key, String type) {
    BasicDBObject whereQuery = new BasicDBObject();
    whereQuery.put(key, type);
    DBCursor cursor = getConnectCollection().find(whereQuery);
    return cursor.toArray();
  }

  public boolean getRecordStatus(String key, String value) {
    collection = getConnectCollection();
    BasicDBObject whereQuery = new BasicDBObject();
    whereQuery.put(key, value);
    DBCursor cursor = collection.find(whereQuery);
    return cursor.hasNext();
  }

  public DBCollection getConnectCollection() {
    return this.collection;
  }

  /*
   * Getting all records from a collection
   */
  public List<DBObject> getCollectionRecords() {
    List<DBObject> cursor = getConnectCollection().find().toArray();
    return cursor;
  }

  /*
   * Connects to a database and provides the available collections
   */
  public void deleteRecord(String key, Object type) {
    collection = getConnectCollection();
    BasicDBObject whereQuery = new BasicDBObject();
    whereQuery.put(key, type);
    collection.remove(whereQuery);
  }

  public void deleteCollection() {
    getConnectCollection().drop();
  }

  public List<String> gettingAllDbs() {
    List<String> dbs = client.getDatabaseNames();
    return dbs;
  }

  public Set<String> gettingAllCollections(String dbname) {
    Set<String> collections = client.getDB(dbname).getCollectionNames();
    return collections;
  }

}