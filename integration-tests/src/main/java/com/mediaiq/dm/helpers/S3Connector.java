package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Connector {
  private static List<Date> date = new ArrayList<Date>();
  private static List<S3ObjectSummary> summery = new ArrayList<S3ObjectSummary>();
  private static List<String> keys = new ArrayList<String>();
  private static S3Connector s3connector = new S3Connector();
  private static List<String> values = new ArrayList<String>();
  private static List<S3ObjectSummary> manifestsummery = new ArrayList<S3ObjectSummary>();
  private static List<S3ObjectSummary> foldersummery = new ArrayList<S3ObjectSummary>();
  private static List<String> manifestkeys = new ArrayList<String>();
  private static List<String> folderkeys = new ArrayList<String>();
  private static List<String> filenames = new ArrayList<String>();
  public static String dcmpre = null;
  public static String miqprofilekey = null;
  public static String naprofilekey = null;
  public static String miqfilename = null;
  public static String nafilename = null;
  public static String reportdate = null;
  public static List<String> nullfilenames = new ArrayList<String>();
  public static List<String> nullfiles1stHour = new ArrayList<String>();
  public static List<String> nullfiles7thHour = new ArrayList<String>();
  public static List<String> nullfiles14thHour = new ArrayList<String>();
  public static List<String> corruptFileNames = new ArrayList<String>();
  public static List<String> corrupt1stHour = new ArrayList<String>();
  public static List<String> corrupt7thHour = new ArrayList<String>();
  public static List<String> corrupt14thHour = new ArrayList<String>();

  /*
   * Provides todays date
   */
  public Date getTodaysDate() {
    Date date = new Date();
    return (date);
  }

  /*
   * Connects to s3 and returns a s3 object
   */
  public AmazonS3 connectingToS3() throws IOException, ParseException {
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id,
        Credentials.secret_access_key);
    AmazonS3 s3 = new AmazonS3Client(awsCreds);
    Region usWest2 = Region.getRegion(Regions.US_EAST_1);
    s3.setRegion(usWest2);
    return (s3);
  }

  /*
   * Goes into a s3 folder, fetches the last modified file Returns a filename which can be copied
   */
  public void getFileToCopy(String BucketName, String prefix)
      throws AmazonServiceException, AmazonClientException, IOException, ParseException {
    ListObjectsRequest manifest = new ListObjectsRequest().withBucketName(BucketName)
        .withPrefix(prefix);
    ObjectListing manifestlisting = connectingToS3().listObjects(manifest);
    for (S3ObjectSummary a : manifestlisting.getObjectSummaries()) {
      foldersummery.add(a);
      folderkeys.add(a.getKey());
    }
    String latestfile = folderkeys.get(folderkeys.size() - 1);
  }

  /*
   * Perform the s3 copy option
   */
  public void CopyS3ToS3(String sourceBucket, String sourceFile, String targetBucket,
      String targetFile)
      throws AmazonServiceException, AmazonClientException, IOException, ParseException {
    CopyObjectRequest copyObjRequest = new CopyObjectRequest(sourceBucket, sourceFile, targetBucket,
        targetFile);
    connectingToS3().copyObject(copyObjRequest);
  }

  public void downloadFileFromS3(String bucketname, String key, String fileName)
      throws IOException, ParseException {
    AmazonS3 s3 = connectingToS3();
    S3Object o = s3.getObject(bucketname, key);
    S3ObjectInputStream s3is = o.getObjectContent();
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    byte[] read_buf = new byte[1024];
    int read_len = 0;
    while ((read_len = s3is.read(read_buf)) > 0) {
      fos.write(read_buf, 0, read_len);
    }
    s3is.close();
    fos.close();
  }

  public void getDCMfileName() throws IOException, ParseException {
    ListObjectsV2Result result = getDCMFiles();
    for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
      if (objectSummary.getSize() > 20) {
        if (objectSummary.getKey().contains("miq")) {
          miqprofilekey = objectSummary.getKey();
          String[] y = miqprofilekey.split("/");
          miqfilename = y[y.length - 1];
        } else {
          naprofilekey = objectSummary.getKey();
          String[] x = naprofilekey.split("/");
          nafilename = x[x.length - 1];
        }
      }
    }
  }

  public void DCMSizeValidation() throws IOException, ParseException {
    ListObjectsV2Result result = getDCMFiles();
    for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
      if (objectSummary.getSize() == 20) {
        String[] y = objectSummary.getKey().split("/");
        nullfilenames.add(y[y.length - 1]);
      } else if (objectSummary.getSize() <= 300000 && objectSummary.getSize() >= 20) {
        String[] x = objectSummary.getKey().split("/");
        corruptFileNames.add(x[x.length - 1]);
      }
    }
  }

  public void getHourlyNullFileNames() throws IOException, ParseException {
    for (String a : nullfilenames) {
      if (a.contains("-01")) {
        nullfiles1stHour.add(a);
      } else if (a.contains("-07")) {
        nullfiles7thHour.add(a);
      } else if (a.contains("-14")) {
        nullfiles14thHour.add(a);
      }
    }
  }

  public void getHourlyCorruptFileNames() throws IOException, ParseException {
    for (String a : corruptFileNames) {
      if (a.contains("-01")) {
        corrupt1stHour.add(a);
      } else if (a.contains("-07")) {
        corrupt7thHour.add(a);
      } else if (a.contains("-14")) {
        corrupt14thHour.add(a);
      }
    }
  }

  public ListObjectsV2Result getDCMFiles() throws IOException, ParseException {
    getPastDay(1);
    AmazonS3 s3Client = connectingToS3();
    ListObjectsV2Request req = new ListObjectsV2Request().withBucketName("aiqdatabucket")
        .withPrefix("dcm/dayserial_numeric=" + dcmpre + "/");
    ListObjectsV2Result result = s3Client.listObjectsV2(req);
    return result;
  }

  public String getPastDay(int desiredDate) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -desiredDate);
    String Day = dateFormat.format(calendar.getTime());
    String[] res = Day.split("-");
    dcmpre = res[0].concat(res[1]).concat(res[2]);
    return Day;
  }
}