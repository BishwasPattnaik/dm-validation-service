package com.mediaiq.dm.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;

public class S3ConnectorDCM {
  private static List<Date> date = new ArrayList<Date>();
  private static List<S3ObjectSummary> summery = new ArrayList<S3ObjectSummary>();
  private static List<String> keys = new ArrayList<String>();
  private static S3ConnectorDCM s3connector = new S3ConnectorDCM();
  private static List<String> values = new ArrayList<String>();
  private static List<S3ObjectSummary> manifestsummery = new ArrayList<S3ObjectSummary>();
  private static List<S3ObjectSummary> foldersummery = new ArrayList<S3ObjectSummary>();
  private static List<String> manifestkeys = new ArrayList<String>();
  private static List<String> folderkeys = new ArrayList<String>();
  private static List<String> filenames = new ArrayList<String>();
  public static String dcmpre = null;
  public static String dcmpreyday = null;
  public static String miqprofilekey7thHour = null;
  public static String miqprofilekey14thHour = null;
  public static String naprofilekey7thHour = null;
  public static String naprofilekey14thHour = null;
  public static String miqfilename7thHour = null;
  public static String miqfilename14thHour = null;
  public static String nafilename7thHour = null;
  public static String nafilename14thHour = null;
  public static String reportdate = null;
  public static String reportdateyday = null;

  /*
   * Provides the size of the bucket Takes input the bucketname
   */
  public void getFolderDetails(String bucketname) throws IOException, ParseException {
    ListObjectsRequest req = new ListObjectsRequest().withBucketName(bucketname)
        .withPrefix(bucketname + getPrefix());
    ObjectListing listing = connectingToS3().listObjects(req);
    for (S3ObjectSummary a : listing.getObjectSummaries()) {
      summery.add(a);
    }
  }

  /*
   * Function which gives the difference between current time and the last modified time of the
   * latest file in s3
   */
  public int getDiffOfLastModifiedDate(String filetopick) throws IOException, ParseException {
    Date last = getLastModifiedDate(filetopick);
    Date today = getTodaysDate();
    int diff = (int) (today.getTime() - last.getTime());
    return (diff);
  }

  /*
   * It goes into the s3 folder test/yy/mm/dd and provides the last modified date of files in a s3
   * location
   */
  public Date getLastModifiedDate(String filetopick) throws IOException, ParseException {
    ObjectListing obj = listingObjects(filetopick);
    for (S3ObjectSummary summary : obj.getObjectSummaries()) {
      date.add(summary.getLastModified());
    }
    Collections.sort(keys);
    int lastelement = keys.size() - 1;
    return (date.get(lastelement));
  }

  /*
   * Provides the s3 prefix
   */

  public String getPrefix() {
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Date date = new Date();
    SimpleDateFormat mf = new SimpleDateFormat("MM");
    SimpleDateFormat df = new SimpleDateFormat("dd");
    String month = mf.format(date);
    String day = df.format(date);
    int yday = Integer.parseInt(day) - 1;
    dcmpre = year + month + day;
    String prefix = "year=" + year + "/month=" + month + "/day=" + day + "/";
    reportdate = year + "-" + month + "-" + Integer.toString(yday);
    reportdateyday = year + "-" + month + "-" + Integer.toString(yday - 1);
    return (prefix);
  }

  /*
   * Provides todays date
   */
  public Date getTodaysDate() {
    Date date = new Date();
    return (date);
  }

  /*
   * Provides the size of a s3 bucket/folder
   */
  public int getFolderSize(String filetopick) throws IOException, ParseException {
    ObjectListing obj = listingObjects(filetopick);
    return (obj.getObjectSummaries().size());
  }

  /*
   * Connects to s3 and access a bucket and add all the objects under the bucket into a list
   */
  public ObjectListing listingObjects(String filetopick) throws IOException, ParseException {
    String prefix = s3connector.getTagetLocation(filetopick) + "/" + getPrefix();
    String bucketname = Credentials.bucketname;
    AmazonS3 bucket = connectingToS3();
    ListObjectsRequest req = new ListObjectsRequest().withBucketName(bucketname).withPrefix(prefix);
    return bucket.listObjects(req);
  }

  /*
   * Connects to s3 and returns a s3 object
   */
  public AmazonS3 connectingToS3() throws IOException, ParseException {
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(Credentials.access_key_id,
        Credentials.secret_access_key);
    AmazonS3 s3 = new AmazonS3Client(awsCreds);
    Region usWest2 = Region.getRegion(Regions.US_EAST_1);
    s3.setRegion(usWest2);
    return (s3);
  }

  /*
   * Reads a pipeline post body and selects the target location
   */
  public String getTagetLocation(String filetopick) throws IOException, ParseException {
    JSONParser parser = new JSONParser();
    JSONObject file = (JSONObject) parser
        .parse(new FileReader("src/test/resources/testData/PipelinePostBody/" + filetopick));
    JSONObject location = (JSONObject) file.get("target");
    return location.get("location").toString().replace("/", "");
  }

  /*
   * Takes input the manifest file name and provides a prefix
   */
  public static String getManifestprefix(String manifestfilename) {
    String[] x = manifestfilename.split("-");
    String value = x[2];
    String year = (value.substring(0, 4));
    String month = (value.substring(4, 6));
    String date = (value.substring(6, 8));
    String hour = (value.substring(8, 10));
    String prefix = "year=" + year + "/month=" + month + "/day=" + date + "/hour=" + hour + "/";
    return prefix;
  }

  /*
   * Goes into a s3 folder, fetches the last modified file Returns a filename which can be copied
   */
  public String getFileToCopy(String BucketName, String prefix)
      throws AmazonServiceException, AmazonClientException, IOException, ParseException {
    String keyword = "processed";
    ListObjectsRequest manifest = new ListObjectsRequest().withBucketName(BucketName)
        .withPrefix(prefix);
    ObjectListing manifestlisting = connectingToS3().listObjects(manifest);
    for (S3ObjectSummary a : manifestlisting.getObjectSummaries()) {
      foldersummery.add(a);
      folderkeys.add(a.getKey());
    }
    String latestfile = folderkeys.get(folderkeys.size() - 1);
    String[] x = latestfile.split("/");
    if (latestfile.contains(keyword)) {
      for (int i = 0; i < x.length; i++) {
        if (x[i].equals(keyword) == false) {
          values.add(x[i]);
        }
      }
      String filetocopy = String.join("/", values);
      return filetocopy;
    } else {
      return latestfile;
    }
  }

  /*
   * Returns a manifest file name which can be copied
   */
  public String getManifestFileToCopy(String BucketName, String manifestprefix)
      throws AmazonServiceException, AmazonClientException, IOException, ParseException {
    ListObjectsRequest manifest = new ListObjectsRequest().withBucketName(BucketName)
        .withPrefix(manifestprefix);
    ObjectListing manifestlisting = connectingToS3().listObjects(manifest);
    for (S3ObjectSummary a : manifestlisting.getObjectSummaries()) {
      manifestsummery.add(a);
      manifestkeys.add(a.getKey());
    }
    String latestfile = manifestkeys.get(manifestkeys.size() - 1);
    return latestfile;
  }

  /*
   * Perform the s3 copy option
   */
  public void performCopy(String sourceBucket, String sourceFile, String targetBucket,
      String targetFile)
      throws AmazonServiceException, AmazonClientException, IOException, ParseException {
    CopyObjectRequest copyObjRequest = new CopyObjectRequest(sourceBucket, sourceFile, targetBucket,
        targetFile);
    connectingToS3().copyObject(copyObjRequest);
  }

  public void copyObjectToS3(String reportpath, String prefix) throws IOException, ParseException,
      AmazonServiceException, AmazonClientException, InterruptedException {
    connectingToS3();
    TransferManager tsfer_mngr = new TransferManager();
    String bucketname = "integration-test-report.mediaiqdigital.com";
    MultipleFileUpload tsfer = tsfer_mngr.uploadDirectory(bucketname, prefix, new File(reportpath),
        true);
    tsfer.waitForCompletion();
  }

  public String getReportPrefix() {
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Date date = new Date();
    SimpleDateFormat mf = new SimpleDateFormat("MM");
    SimpleDateFormat df = new SimpleDateFormat("dd");
    String month = mf.format(date);
    String day = df.format(date);
    int hour = LocalDateTime.now().getHour();
    int minutes = LocalDateTime.now().getMinute();
    String prefix = "year=" + year + "/month=" + month + "/day=" + day + "/hour=" + hour
        + "/minutes=" + minutes;
    return (prefix);
  }

  public void downloadFileFromS(String bucketname, String key, String fileName)
      throws IOException, ParseException {
    AmazonS3 s3 = connectingToS3();
    S3Object o = s3.getObject(bucketname, key);
    S3ObjectInputStream s3is = o.getObjectContent();
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    byte[] read_buf = new byte[1024];
    int read_len = 0;
    while ((read_len = s3is.read(read_buf)) > 0) {
      fos.write(read_buf, 0, read_len);
    }
    s3is.close();
    fos.close();
  }

  public ObjectListing listingFolderObjects(String prefix) throws IOException, ParseException {
    String bucketname = Credentials.bucketname;
    AmazonS3 bucket = connectingToS3();
    ListObjectsRequest req = new ListObjectsRequest().withBucketName(bucketname).withPrefix(prefix);
    return bucket.listObjects(req);
  }

  public void getDCMfileName(String reportdate) throws IOException, ParseException {
    getPrefix();
    AmazonS3 s3Client = connectingToS3();
    ListObjectsV2Request req = new ListObjectsV2Request().withBucketName("aiqdatabucket")
        .withPrefix("dcm/dayserial_numeric=" + reportdate + "/");
    ListObjectsV2Result result;
    result = s3Client.listObjectsV2(req);
    for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
      if (objectSummary.getSize() > 20) {
        System.out.println(objectSummary.getKey().toString());
        if (objectSummary.getKey().contains("-07_miq")) {
          miqprofilekey7thHour = objectSummary.getKey();
          String[] y = miqprofilekey7thHour.split("/");
          miqfilename7thHour = y[y.length - 1];
        } else if (objectSummary.getKey().contains("-14_miq")) {
          miqprofilekey14thHour = objectSummary.getKey();
          String[] x = miqprofilekey14thHour.split("/");
          miqfilename14thHour = x[x.length - 1];
        } else if (objectSummary.getKey().contains("-07_na")) {
          naprofilekey7thHour = objectSummary.getKey();
          String[] x = naprofilekey7thHour.split("/");
          nafilename7thHour = x[x.length - 1];
        } else if (objectSummary.getKey().contains("-14_na")) {
          naprofilekey14thHour = objectSummary.getKey();
          String[] x = naprofilekey14thHour.split("/");
          nafilename14thHour = x[x.length - 1];
        }
      }
    }
  }

  public static String getPastDay(int desiredDate) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -desiredDate);
    String Day = dateFormat.format(calendar.getTime());
    String[] res = Day.split("-");
    dcmpreyday = res[0].concat(res[1]).concat(res[2]);
    return Day;
  }

  public static String getPastDayDCM(int desiredDate) {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -desiredDate);
    String Day = dateFormat.format(calendar.getTime());
    return Day;
  }
}