package com.mediaiq.dm.helpers;

import java.util.ArrayList;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SftpConnector {

  public static Session session = null;
  public static Channel channel = null;
  public static ChannelSftp channelSftp = null;

  public SftpConnector(String sftphost, String sftpuser, String sftppass)
      throws JSchException, SftpException {
    JSch jsch = new JSch();
    session = jsch.getSession(sftpuser, sftphost);
    session.setPassword(sftppass);
    java.util.Properties config = new java.util.Properties();
    config.put("StrictHostKeyChecking", "no");
    session.setConfig(config);
    session.connect();
    channel = session.openChannel("sftp");
    channel.connect();
    channelSftp = (ChannelSftp) channel;
  }

  public boolean gettingFileStatus(String sftpworkingdir, String fileformat)
      throws SftpException, JSchException {
    Vector filelist = channelSftp.ls(sftpworkingdir);
    ArrayList<Boolean> readystatus = new ArrayList<>();
    for (int i = 0; i < filelist.size(); i++) {
      readystatus.add(filelist.get(i).toString().contains(fileformat));
    }
    return (readystatus.contains(true));
  }
}
