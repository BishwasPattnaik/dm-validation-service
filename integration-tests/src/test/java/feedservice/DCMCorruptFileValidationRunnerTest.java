package feedservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import com.mediaiq.dm.helpers.MailUtilCorrupt;
import com.mediaiq.dm.helpers.S3Connector;


public class DCMCorruptFileValidationRunnerTest {
  private static List<String> nullfilenames = new ArrayList<String>();
  private static List<String> nullfiles1stHour = new ArrayList<String>();
  private static List<String> nullfiles7thHour = new ArrayList<String>();
  private static List<String> nullfiles14thHour = new ArrayList<String>();
  private static List<String> corruptFileNames = new ArrayList<String>();
  private static List<String> corrupt1stHour = new ArrayList<String>();
  private static List<String> corrupt7thHour = new ArrayList<String>();
  private static List<String> corrupt14thHour = new ArrayList<String>();

  @Test
  public void runner() throws IOException, ParseException {
    S3Connector s3obj = new S3Connector();
    s3obj.DCMSizeValidation();
    s3obj.getHourlyNullFileNames();
    s3obj.getHourlyCorruptFileNames();
    nullfilenames = s3obj.nullfilenames;
    nullfiles1stHour = s3obj.nullfiles1stHour;
    nullfiles7thHour = s3obj.nullfiles7thHour;
    nullfiles14thHour = s3obj.nullfiles14thHour;
    corruptFileNames = s3obj.corruptFileNames;
    corrupt1stHour = s3obj.corrupt1stHour;
    corrupt7thHour = s3obj.corrupt7thHour;
    corrupt14thHour = s3obj.corrupt14thHour;
    MailUtilCorrupt mu = new MailUtilCorrupt();
    /*
     * if (nullfiles1stHour.size() != 0 && nullfiles7thHour.size() != 0) {
     * mu.sendEmail("bishwas@miqdigital.com", "DCM empty FIle Details",
     * "Hi Team, \nPlease find the file detals which are empty,\n" +
     * "For the 1st hour empty files are : \n" + nullfiles1stHour +
     * "\nAnd For the 7th hour empty files are : \n" + nullfiles7thHour); } else if
     * (nullfiles1stHour.size() != 0) { mu.sendEmail("bishwas@miqdigital.com",
     * "DCM empty FIle Details", "Hi Team, \nPlease find the file detals which are empty,\n" +
     * "For the 1st hour file empty files are : \n" + nullfiles1stHour); }
     */
    if (nullfiles7thHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM empty File Details", "Hi Team, \nPlease find the file detals which are empty,\n"
              + "For the 7th hour file empty files are : \n" + nullfiles7thHour);
    } else if (nullfiles14thHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM empty FIle Details", "Hi Team, \nPlease find the file detals which are empty,\n"
              + "For the 14th hour file empty files are : \n" + nullfiles14thHour);
    }
    if (corrupt7thHour.size() != 0 && corrupt14thHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM corrupt FIle Details",
          "Hi Team, \nPlease find the file detals which have less than 300KB,\n"
              + "\nAnd For the 7th hour corrupt files are : \n" + corrupt7thHour
              + "\nAnd For the 14th hour corrupt files are : \n" + corrupt14thHour);
    } else if (corrupt1stHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM corrupt FIle Details",
          "Hi Team, \nPlease find the file detals which have less than 300KB,\n"
              + "For the 1st hour file corrupt files are : \n" + corrupt1stHour);
    } else if (corrupt7thHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM corrupt FIle Details",
          "Hi Team, \nPlease find the file detals which have less than 300KB,\n"
              + "For the 7th hour file corrupt files are : \n" + corrupt7thHour);
    } else if (corrupt14thHour.size() != 0) {
      mu.sendEmailDCM("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
          "DCM corrupt FIle Details",
          "Hi Team, \nPlease find the file detals which have less than 300KB,\n"
              + "For the 14th hour file corrupt files are : \n" + corrupt14thHour);
    }
  }
}
