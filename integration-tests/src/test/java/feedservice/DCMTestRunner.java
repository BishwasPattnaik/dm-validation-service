package feedservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.mail.MessagingException;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import com.mediaiq.dm.helpers.CreateHtmlpage;
import com.mediaiq.dm.helpers.DCMProcessing;
import com.mediaiq.dm.helpers.FileHandlingUtils;
import com.mediaiq.dm.helpers.MailUtil;
import com.mediaiq.dm.helpers.S3ConnectorDCM;

public class DCMTestRunner {

  @Test
  public void runner() throws ClassNotFoundException, SQLException, IOException,
      InterruptedException, ParseException, MessagingException {
    String[] profiles = { "miq" };
    for (String a : profiles) {
      DCMProcessing.runValidationDCMConsole(a);
      DCMProcessing.runValidationS3(a);
      String htmlBody = CreateHtmlpage.createReportHtml("DCMAdvCount.csv",
          "AdvertiserIdCountS3.csv");
      int dcmcount = FileHandlingUtils.getTotalDcmCount();
      int s3count = FileHandlingUtils.getTotalS3Count();
      float diff = dcmcount - s3count;
      float percentdiff = ((diff / s3count) * 100);
      if (percentdiff > 2.0 || percentdiff < -2.0 || percentdiff == 0.0) {
        String mailbody = "Hi Team, \n"
            + "The record count in files downloaded by API and the record count in s3-aiqdatabucket file, for :"
            + S3ConnectorDCM.getPastDayDCM(2)
            + " date is with in the acceptance criteria.(i.e within 2% difference)";
        MailUtil.sendEmailHtml("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
            "DCM Automation Report : PASSED",
            "Profile : " + a.toUpperCase() + "- " + S3ConnectorDCM.getPastDayDCM(2), mailbody,
            htmlBody + "\nThanks.");
      } else {
        String mailbody = "Hi Team, \n" + "The count in DCM console and the s3 for "
            + S3ConnectorDCM.getPastDayDCM(2) + " date is not matching, " + "\n as it is having "
            + percentdiff + "% difference in count ." + "\n" + "\n S3 record count : " + s3count
            + "\n DCM console record count : " + dcmcount + "\n" + "\n We are looking into it."
            + "\n In case of any support please contact :s4@miqdigital.com "
            + "\n And below is the detail report of advetiser-id count in DCM console and S3, \n";
        MailUtil.sendEmailHtml("s4@miqdigital.com", "jarvis-connect@miqdigital.com",
            "DCM Automation Report : FAILED",
            "Profile : " + a.toUpperCase() + "- " + S3ConnectorDCM.getPastDayDCM(2), mailbody,
            htmlBody + "\nThanks.");
      }
      FileHandlingUtils.deleteFiles("AdvertiserIdCountS3.csv");
      FileHandlingUtils.deleteFiles("AdvCount.csv");
      FileHandlingUtils.deleteFiles("count.csv");
    }
  }
}
