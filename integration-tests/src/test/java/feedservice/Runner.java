package feedservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.Test;

import com.mediaiq.dm.helpers.ApachePOIExcelRead;
import com.mediaiq.dm.helpers.JdbcUtil;
import com.mediaiq.dm.helpers.MailUtil;

public class Runner {
  public static List<Double> tablesizes = new ArrayList<>();
  public static List<Double> totalrows = new ArrayList<>();
  public static List<Double> diffofsizes = new ArrayList<>();
  private static String FILE_NAME = System.getProperty("user.dir")
      + "/src/test/resources/Report.xlsx";

  @Test
  public void runTest()
      throws ClassNotFoundException, SQLException, IOException, InterruptedException {
    JdbcUtil jdbcobj = new JdbcUtil(FILE_NAME);
    jdbcobj.getTableSize("public", "mbytes", "rows");
    tablesizes = jdbcobj.tablesizes;
    totalrows = jdbcobj.totalrows;
    ApachePOIExcelRead obj = new ApachePOIExcelRead();
    obj.createColumn(FILE_NAME, "Size in MBs till : ");
    obj.writeXLSXFile(FILE_NAME, tablesizes);
    obj.createColumn(FILE_NAME, "Total number of rows till : ");
    obj.writeXLSXFile(FILE_NAME, totalrows);
    obj.calculateDiff(FILE_NAME);
  }

  @AfterClass
  public static void Test() throws IOException, ParseException, InterruptedException {
    String body = "Hi Team," + "Please find the attached sheet for the feed details in Redshift ";
    MailUtil.sendEmailFeed("s4@miqdigital.com", "Feed Reports", body, FILE_NAME);
  }
}
